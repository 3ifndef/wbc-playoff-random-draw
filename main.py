import pygame
import time
import random
from copy import deepcopy

SCREEN_W = 800
SCREEN_H = 600

class Animation:
    def __init__(self):
        self.in_progress = False
        self.started_at = 0
        self.duration = 0

    def start(self, duration, start_delay):
        self.in_progress = True
        self.started_at = time.time() + start_delay
        self.duration = duration

    def update(self):
        if not self.in_progress:
            return 0
        progress = (time.time() - self.started_at) / self.duration
        if progress >= 1:
            self.in_progress = False
            return 1
        return max(0, progress)

class Players:
    def __init__(self, players, size):
        self.players = players
        self.size = size
        self.player_texts = [my_font.render(p, False, (0, 0, 0)) for p in players]
        self.text_height = [sum(pt.get_height() for pt in self.player_texts[b * self.size : (b + 1) * self.size]) for b in range(2)]
        self.spacing = [(SCREEN_H - self.text_height[b]) / (self.size + 1) for b in range(2)]
        self.ys = [[self.spacing[b] * (i + 1) + sum(pt.get_height() for pt in self.player_texts[b * self.size : b * self.size + i]) for i in range(self.size)] for b in range(2)]
        self.state = [list(range(self.size)) for b in range(2)]
        self.state_target = self.state
        self.animation = Animation()

    def draw(self):
        anim = self.animation.update()
        for b in range(2):
            x = 100 + b * 400
            for i in range(self.size):
                screen.blit(self.player_texts[b * self.size + i], (x, self.ys[b][self.state[b][i]] * (1 - anim) + self.ys[b][self.state_target[b][i]] * anim))
        for i in range(self.size):
            efrom = self.state_target[0].index(i)
            eto = self.state_target[1].index(i)
            yfrom = self.ys[0][self.state[0][efrom]] * (1 - anim) + self.ys[0][self.state_target[0][efrom]] * anim + self.player_texts[efrom].get_height() / 2
            yto = self.ys[1][self.state[1][eto]] * (1 - anim) + self.ys[1][self.state_target[1][eto]] * anim + self.player_texts[self.size + eto].get_height() / 2
            pygame.draw.line(screen, (0, 0, 100), (100 + self.player_texts[efrom].get_width() + 20, yfrom), (500 - 20, yto), 5)
        if anim == 1:
            self.state = self.state_target

    def impossible_target(self):
        for i in range(self.size):
            if self.state_target[0].index(i) == self.state_target[1].index(i):
                return True
        return False

    def shuffle_animated(self, duration, start_delay):
        self.state_target = deepcopy(self.state)
        while self.impossible_target():
            for b in range(2):
                random.shuffle(self.state_target[b])
        self.animation.start(duration, start_delay)

class Game:
    def __init__(self):
        with open('config.txt', encoding='utf8') as config_file:
            lines = config_file.read().strip().split('\n')
            title = lines[0].strip()
            size = int(lines[1])
            n_players = len(lines) - 2
            players = []
            for b in range(2):
                for i in range(size):
                    id = b * size + i
                    players.append(lines[id + 2].strip() if id < n_players else '(bye)')

        pygame.display.set_caption(title)

        self.players = Players(players, size)
        self.started_at = time.time()
        self.state = 0

        self.current_a = 0
        self.current_b = 0
        self.tm_last_line = 0

    def process_event(self, event):
        if event.type == pygame.MOUSEBUTTONUP and self.state == 0:
            self.players.shuffle_animated(5, 2)
            self.state = 1

    def update(self):
        pass

    def draw(self):
        self.players.draw()
        if self.state in (2, 3):
            pygame.draw.line(screen, (0, 0, 100), (100 + self.players.player_texts[self.current_a].get_width() + 50, self.players.ys[0][self.current_a] + 20), (450, self.players.ys[1][self.current_b] + 20), 5)


pygame.init()
pygame.font.init()

my_font = pygame.font.SysFont('Comic Sans MS', 20)

screen = pygame.display.set_mode((SCREEN_W, SCREEN_H))
done = False

game = Game()

while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        else:
            game.process_event(event)

    screen.fill((255, 255, 255))
    game.update()
    game.draw()
    pygame.display.flip()